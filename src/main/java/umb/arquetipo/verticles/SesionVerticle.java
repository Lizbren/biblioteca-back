package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.sesion.SesionService;

public class SesionVerticle extends AbstractVerticle {
    SesionService service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = SesionService.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("sesion-service")
                .register(SesionService.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
