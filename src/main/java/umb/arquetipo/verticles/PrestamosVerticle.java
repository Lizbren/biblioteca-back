package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.prestamos.PrestamosService;

public class PrestamosVerticle extends AbstractVerticle {
    PrestamosService service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = PrestamosService.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("prestamos-service")
                .register(PrestamosService.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
