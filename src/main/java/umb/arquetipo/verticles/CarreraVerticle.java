package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.carrera.CarreraService;
import umb.arquetipo.micro.editoriales.EditorialesService;

public class CarreraVerticle extends AbstractVerticle {
    CarreraService service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = CarreraService.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("carrera-service")
                .register(CarreraService.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
