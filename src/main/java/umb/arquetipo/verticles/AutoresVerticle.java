package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.autores.AutoresService;

public class AutoresVerticle extends AbstractVerticle {
    AutoresService service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = AutoresService.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("autores-service")
                .register(AutoresService.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
