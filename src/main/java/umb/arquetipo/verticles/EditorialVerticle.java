package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.autores.AutoresService;
import umb.arquetipo.micro.editoriales.EditorialesService;

public class EditorialVerticle extends AbstractVerticle {
    EditorialesService service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = EditorialesService.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("editorial-service")
                .register(EditorialesService.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
