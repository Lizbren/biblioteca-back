package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.categorias.CategoriasService;

public class CategoriasVerticle extends AbstractVerticle {
    CategoriasService service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = CategoriasService.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("categorias-service")
                .register(CategoriasService.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
