package umb.arquetipo.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import umb.arquetipo.micro.libros.LibrosService;

public class LibroVerticle extends AbstractVerticle {
    LibrosService service;
    private MessageConsumer<JsonObject> binder;

    @Override
    public void start() {
        this.service = LibrosService.create(vertx, config());
        this.binder = new ServiceBinder(vertx)
                .setAddress("libros-service")
                .register(LibrosService.class, this.service);
    }

    @Override
    public void stop() {
        this.binder.unregister();
    }
}
