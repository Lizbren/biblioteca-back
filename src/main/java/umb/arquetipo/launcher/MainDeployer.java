package umb.arquetipo.launcher;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.verticles.AppVerticle;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainDeployer extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(MainDeployer.class);
    private static final List<String> CATALOGE_SERVICE_KEYS = Collections.unmodifiableList(Arrays.asList("UMB_PG_DB_PORT", "UMB_PG_DB_HOST", "UMB_PG_DB_NAME", "UMB_PG_DB_USER", "UMB_PG_DB_PASS", "UMB_PG_MAX_POOL_SIZE", "GRAPHIQL"));

    private static final String AUTORES_MICRO_SERVICE_VERTICLE = "umb.arquetipo.verticles.AutoresVerticle";
    private static final String SESION_MICRO_SERVICE_VERTICLE = "umb.arquetipo.verticles.SesionVerticle";
    private static final String CATEGORIAS_MICRO_SERVICE_VERTICLE = "umb.arquetipo.verticles.CategoriasVerticle";
    private static final String EDITORIAL_MICRO_SERVICE_VERTICLE = "umb.arquetipo.verticles.EditorialVerticle";
    private static final String CARRERA_MICRO_SERVICE_VERTICLE = "umb.arquetipo.verticles.CarreraVerticle";
    private static final String LIBRO_MICRO_SERVICE_VERTICLE = "umb.arquetipo.verticles.LibroVerticle";
    private static final String PRESTAMO_MICRO_SERVICE_VERTICLE = "umb.arquetipo.verticles.PrestamosVerticle";



    private static final String VERTX_START = "Vertx iniciado";
    private static final String VERTX_FAILED = "Vertx fallo: ";
    private static final String APP_NAME = "catalogos";
    private static final String JSON = "json";
    private static final String APP_NAME_TEXT = "appName";
    private static final String TIME_OUT = "timeout";
    private static final String ENV = "env";
    private static final String MASSIVE_SERVICE_KEYS_TEXT = "keys";

    @Override
    public void start() {
        getConfiguration()
                .onSuccess(t -> logger.info(VERTX_START))
                .onFailure(e -> logger.error(VERTX_FAILED + e.getMessage()));
    }

    private Future<String> getConfiguration() {
        return ConfigRetriever.create(vertx, createConfigRetrieverOptions())
                .getConfig()
                .map(this::createConfig)
                .compose(this::deployVerticles);
    }

    private JsonArray getKeys() {
        JsonArray configuration = new JsonArray();
        CATALOGE_SERVICE_KEYS.forEach(configuration::add);
        return configuration;
    }

    private ConfigRetrieverOptions createConfigRetrieverOptions() {
        return new ConfigRetrieverOptions()
                .addStore(new ConfigStoreOptions()
                        .setType(JSON)
                        .setFormat(JSON)
                        .setConfig(new JsonObject().put(APP_NAME_TEXT, APP_NAME).put(TIME_OUT, 90000L)))
                .addStore(new ConfigStoreOptions()
                        .setType(ENV)
                        .setConfig(new JsonObject().put(MASSIVE_SERVICE_KEYS_TEXT, getKeys())));
    }

    private DeploymentOptions createConfig(JsonObject configurations) {
        return new DeploymentOptions()
                .setConfig(configurations)
                .setWorker(true);
    }

    private Future<String> deployVerticles(DeploymentOptions options) {
        return vertx.deployVerticle(AUTORES_MICRO_SERVICE_VERTICLE, options)
                .compose(s -> vertx.deployVerticle(SESION_MICRO_SERVICE_VERTICLE, options))
                .compose(s -> vertx.deployVerticle(CATEGORIAS_MICRO_SERVICE_VERTICLE, options))
                .compose(s -> vertx.deployVerticle(EDITORIAL_MICRO_SERVICE_VERTICLE, options))
                .compose(s -> vertx.deployVerticle(CARRERA_MICRO_SERVICE_VERTICLE, options))
                .compose(s -> vertx.deployVerticle(LIBRO_MICRO_SERVICE_VERTICLE, options))
                .compose(s -> vertx.deployVerticle(PRESTAMO_MICRO_SERVICE_VERTICLE, options))
                .compose(s -> vertx.deployVerticle(new AppVerticle(), options));

    }

}
