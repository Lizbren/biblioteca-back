package umb.arquetipo.graphql;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.handler.graphql.schema.VertxDataFetcher;
import umb.arquetipo.micro.autores.AutoresService;
import umb.arquetipo.micro.carrera.CarreraService;
import umb.arquetipo.micro.dto.*;
import umb.arquetipo.micro.categorias.CategoriasService;
import umb.arquetipo.micro.editoriales.EditorialesService;
import umb.arquetipo.micro.libros.LibrosService;
import umb.arquetipo.micro.prestamos.PrestamosService;
import umb.arquetipo.micro.sesion.SesionService;
import umb.arquetipo.micro.dto.SesionDTO;

import java.util.List;

public class GraphQLDataFetchers {

    AutoresService autorService;
    SesionService sesionService;
    CategoriasService categoriasService;
    EditorialesService editorialesService;
    CarreraService carreraService;
    LibrosService librosService;
    PrestamosService prestamosService;
    Vertx vertx;

    public void init(Vertx vertx, JsonObject configuration) {
        this.vertx = vertx;
        autorService = AutoresService
                .createProxy(vertx, "autores-service", new DeliveryOptions().setSendTimeout(120000L));
        sesionService = SesionService
                .createProxy(vertx, "sesion-service", new DeliveryOptions().setSendTimeout(120000L));

        categoriasService = CategoriasService
                .createProxy(vertx, "categorias-service", new DeliveryOptions().setSendTimeout(120000L));
        editorialesService = EditorialesService
                .createProxy(vertx, "editorial-service", new DeliveryOptions().setSendTimeout(120000L));
        carreraService = CarreraService
                .createProxy(vertx, "carrera-service", new DeliveryOptions().setSendTimeout(120000L));
        librosService = LibrosService
                .createProxy(vertx, "libros-service", new DeliveryOptions().setSendTimeout(120000L));

        prestamosService = PrestamosService
                .createProxy(vertx, "prestamos-service", new DeliveryOptions().setSendTimeout(120000L));


    }

    private final VertxDataFetcher<Integer> nuevoAutor = VertxDataFetcher.create((env, handler) ->
            autorService.nuevoAutor(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<List<AutoresDTO>> autores = VertxDataFetcher.create((env, handler) ->
            autorService.autores().onComplete(handler));

    private final VertxDataFetcher<Integer> actualizaAutor = VertxDataFetcher.create((env, handler) ->
            autorService.actualizaAutor(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizaVariosAutores = VertxDataFetcher.create((env, handler) ->
            autorService.actualizaVariosAutores(new JsonObject(env.getArguments())).onComplete(handler));
    private final VertxDataFetcher<Integer> actualizaVigenciaAutor = VertxDataFetcher.create((env, handler) ->
            autorService.actualizaVigenciaAutores(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<SesionDTO> sesion = VertxDataFetcher.create((env, handler) ->
            sesionService.loginUsuario(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> nuevaCategoria = VertxDataFetcher.create((env, handler) ->
            categoriasService.nuevaCategoria(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<List<CategoriasDTO>> categorias = VertxDataFetcher.create((env, handler) ->
            categoriasService.categorias().onComplete(handler));


    private final VertxDataFetcher<Integer> actualizaCategoria = VertxDataFetcher.create((env, handler) ->
            categoriasService.actualizaCategoria(new JsonObject(env.getArguments())).onComplete(handler));


    private final VertxDataFetcher<Integer> actualizaVariasCategorias = VertxDataFetcher.create((env, handler) ->
            categoriasService.actualizaVariasCategorias(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizaVigenciaCategoria = VertxDataFetcher.create((env, handler) ->
            categoriasService.actualizaVigenciaCategoria(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> nuevaEditorial = VertxDataFetcher.create((env, handler) ->
            editorialesService.nuevaEditorial(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<List<EditorialesDTO>> editoriales = VertxDataFetcher.create((env, handler) ->
            editorialesService.editoriales().onComplete(handler));


    private final VertxDataFetcher<Integer> actualizaEditorial = VertxDataFetcher.create((env, handler) ->
            editorialesService.actualizaEditorial(new JsonObject(env.getArguments())).onComplete(handler));


    private final VertxDataFetcher<Integer> actualizaVariasEditoriales = VertxDataFetcher.create((env, handler) ->
            editorialesService.actualizaVariasEditoriales(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizaVigenciaEditorial = VertxDataFetcher.create((env, handler) ->
            editorialesService.actualizaVigenciaEditoriales(new JsonObject(env.getArguments())).onComplete(handler));
    private final VertxDataFetcher<Integer> nuevaCarrera = VertxDataFetcher.create((env, handler) ->
            carreraService.nuevaCarrera(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<List<CarreraDTO>> carreras = VertxDataFetcher.create((env, handler) ->
            carreraService.carreras().onComplete(handler));


    private final VertxDataFetcher<Integer> actualizaCarrera = VertxDataFetcher.create((env, handler) ->
            carreraService.actualizaCarrera(new JsonObject(env.getArguments())).onComplete(handler));


    private final VertxDataFetcher<Integer> actualizaVariasCarreras = VertxDataFetcher.create((env, handler) ->
            carreraService.actualizaVariasCarreras(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizaVigenciaCarrera= VertxDataFetcher.create((env, handler) ->
            carreraService.actualizaVigenciaCarrera(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> nuevoLibro= VertxDataFetcher.create((env, handler) ->
            librosService.nuevoLibro(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<List<LibrosDTO>> libros = VertxDataFetcher.create((env, handler) ->
            librosService.libros().onComplete(handler));

    private final VertxDataFetcher<Integer> actualizalibro = VertxDataFetcher.create((env, handler) ->
            librosService.actualizaLibro(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> solicitarPrestamo = VertxDataFetcher.create((env, handler) ->
            prestamosService.solicitarPrestamo(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<List<PrestamoDTO>> prestamosPorUsuario = VertxDataFetcher.create((env, handler) ->
            prestamosService.prestamosPorUsuario(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> autorizarPrestamo = VertxDataFetcher.create((env, handler) ->
            prestamosService.autorizarPrestamo(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<List<PrestamoDTO>> prestamos = VertxDataFetcher.create((env, handler) ->
            prestamosService.prestamos().onComplete(handler));

    private final VertxDataFetcher<Integer> renovarPrestamo = VertxDataFetcher.create((env, handler) ->
            prestamosService.renovarPrestamo(new JsonObject(env.getArguments())).onComplete(handler));
    private final VertxDataFetcher<Integer> finalizarPrestamo = VertxDataFetcher.create((env, handler) ->
            prestamosService.finalizarPrestamo(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizarDeudaPorUsuario = VertxDataFetcher.create((env, handler) ->
            prestamosService.actualizarDeudaPorUsuario(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizarDeudasUsuarios = VertxDataFetcher.create((env, handler) ->
            prestamosService.actualizarDeudasUsuarios().onComplete(handler));

    private final VertxDataFetcher<Integer> actualizarVencimientoSolicitudPorUsuario = VertxDataFetcher.create((env, handler) ->
            prestamosService.actualizarVencimientoSolicitudPorUsuario(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizarVencimientoSolicitudes = VertxDataFetcher.create((env, handler) ->
            prestamosService.actualizarVencimientoSolicitudes().onComplete(handler));

    private final VertxDataFetcher<Integer> noAutorizarPrestamo = VertxDataFetcher.create((env, handler) ->
            prestamosService.noAutorizarPrestamo(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> nuevoUsuario = VertxDataFetcher.create((env, handler) ->
            sesionService.nuevoUsuario(new JsonObject(env.getArguments())).onComplete(handler));

    private final VertxDataFetcher<Integer> actualizaClave = VertxDataFetcher.create((env, handler) ->
            sesionService.actualizarPass(new JsonObject(env.getArguments())).onComplete(handler));
    private final VertxDataFetcher<List<SesionDTO>> usuarios = VertxDataFetcher.create((env, handler) ->
            sesionService.usuarios().onComplete(handler));

    public VertxDataFetcher<Integer> getActualizaVigenciaCategoria() {
        return actualizaVigenciaCategoria;
    }

    public VertxDataFetcher<List<CategoriasDTO>> getCategorias() {
        return categorias;
    }

    public VertxDataFetcher<Integer> getActualizaCategoria() {
        return actualizaCategoria;
    }

    public VertxDataFetcher<Integer> getActualizaVariasCategorias() {
        return actualizaVariasCategorias;
    }

    public VertxDataFetcher<Integer> getNuevoAutor() {
        return nuevoAutor;
    }

    public VertxDataFetcher<SesionDTO> getSesion() {
        return sesion;
    }

    public VertxDataFetcher<List<AutoresDTO>> getAutores() {
        return autores;
    }

    public VertxDataFetcher<Integer> getActualizaAutor() {
        return actualizaAutor;
    }

    public VertxDataFetcher<Integer> getActualizaVariosAutores() {
        return actualizaVariosAutores;
    }

    public VertxDataFetcher<Integer> getNuevaCategoria() {
        return nuevaCategoria;
    }

    public VertxDataFetcher<Integer> getActualizaVigenciaAutor() {
        return actualizaVigenciaAutor;
    }

    public VertxDataFetcher<Integer> getNuevaEditorial() {
        return nuevaEditorial;
    }

    public VertxDataFetcher<List<EditorialesDTO>> getEditoriales() {
        return editoriales;
    }

    public VertxDataFetcher<Integer> getActualizaEditorial() {
        return actualizaEditorial;
    }

    public VertxDataFetcher<Integer> getActualizaVariasEditoriales() {
        return actualizaVariasEditoriales;
    }

    public VertxDataFetcher<Integer> getActualizaVigenciaEditorial() {
        return actualizaVigenciaEditorial;
    }

    public VertxDataFetcher<Integer> getNuevaCarrera() {
        return nuevaCarrera;
    }

    public VertxDataFetcher<List<CarreraDTO>> getCarreras() {
        return carreras;
    }

    public VertxDataFetcher<Integer> getActualizaCarrera() {
        return actualizaCarrera;
    }

    public VertxDataFetcher<Integer> getActualizaVariasCarreras() {
        return actualizaVariasCarreras;
    }

    public VertxDataFetcher<Integer> getActualizaVigenciaCarrera() {
        return actualizaVigenciaCarrera;
    }

    public VertxDataFetcher<Integer> getNuevoLibro() {
        return nuevoLibro;
    }

    public VertxDataFetcher<List<LibrosDTO>> getLibros() {
        return libros;
    }

    public VertxDataFetcher<Integer> getActualizalibro() {
        return actualizalibro;
    }

    public VertxDataFetcher<Integer> getNuevoUsuario() {
        return nuevoUsuario;
    }

    public VertxDataFetcher<Integer> getSolicitarPrestamo() {
        return solicitarPrestamo;
    }

    public VertxDataFetcher<List<PrestamoDTO>> getPrestamosPorUsuario() {
        return prestamosPorUsuario;
    }

    public VertxDataFetcher<List<PrestamoDTO>> getPrestamos() {
        return prestamos;
    }

    public VertxDataFetcher<Integer> getAutorizarPrestamo() {
        return autorizarPrestamo;
    }

    public VertxDataFetcher<Integer> getRenovarPrestamo() {
        return renovarPrestamo;
    }

    public VertxDataFetcher<Integer> getActualizarDeudaPorUsuario() {
        return actualizarDeudaPorUsuario;
    }

    public VertxDataFetcher<Integer> getActualizarDeudasUsuarios() {
        return actualizarDeudasUsuarios;
    }

    public VertxDataFetcher<Integer> getActualizarVencimientoSolicitudPorUsuario() {
        return actualizarVencimientoSolicitudPorUsuario;
    }

    public VertxDataFetcher<Integer> getActualizarVencimientoSolicitudes() {
        return actualizarVencimientoSolicitudes;
    }

    public VertxDataFetcher<Integer> getFinalizarPrestamo() {
        return finalizarPrestamo;
    }

    public VertxDataFetcher<Integer> getNoAutorizarPrestamo() {
        return noAutorizarPrestamo;
    }

    public VertxDataFetcher<Integer> getActualizaClave() {
        return actualizaClave;
    }

    public VertxDataFetcher<List<SesionDTO>> getUsuarios() {
        return usuarios;
    }
}
