package umb.arquetipo.micro.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject(generateConverter = true)
public class EditorialesDTO {
    private Integer idEditorial;
    private String nombreMay;
    private String nombreMin;
    private Boolean vigente;

    public EditorialesDTO(JsonObject json) {
        EditorialesDTOConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        EditorialesDTOConverter.toJson(this, json);
        return json;
    }

    public Integer getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(Integer idEditorial) {
        this.idEditorial = idEditorial;
    }

    public String getNombreMay() {
        return nombreMay;
    }

    public void setNombreMay(String nombreMay) {
        this.nombreMay = nombreMay;
    }

    public String getNombreMin() {
        return nombreMin;
    }

    public void setNombreMin(String nombreMin) {
        this.nombreMin = nombreMin;
    }

    public Boolean getVigente() {
        return vigente;
    }

    public void setVigente(Boolean vigente) {
        this.vigente = vigente;
    }
}
