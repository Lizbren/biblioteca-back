package umb.arquetipo.micro.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject(generateConverter = true)
public class CarreraDTO {
    private Integer idCarrera;
    private String nombreMay;
    private String nombreMin;
    private Boolean vigente;

    public CarreraDTO(JsonObject json) {
        CarreraDTOConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        CarreraDTOConverter.toJson(this, json);
        return json;
    }

    public Integer getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(Integer idCarrera) {
        this.idCarrera = idCarrera;
    }

    public String getNombreMay() {
        return nombreMay;
    }

    public void setNombreMay(String nombreMay) {
        this.nombreMay = nombreMay;
    }

    public String getNombreMin() {
        return nombreMin;
    }

    public void setNombreMin(String nombreMin) {
        this.nombreMin = nombreMin;
    }

    public Boolean getVigente() {
        return vigente;
    }

    public void setVigente(Boolean vigente) {
        this.vigente = vigente;
    }
}
