package umb.arquetipo.micro.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject(generateConverter = true)
public class CategoriasDTO {
    private Integer idCategoria;
    private String nombreMay;
    private String nombreMin;
    private Boolean vigente;

    public CategoriasDTO(JsonObject json) {
        CategoriasDTOConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        CategoriasDTOConverter.toJson(this, json);
        return json;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombreMay() {
        return nombreMay;
    }

    public void setNombreMay(String nombreMay) {
        this.nombreMay = nombreMay;
    }

    public String getNombreMin() {
        return nombreMin;
    }

    public void setNombreMin(String nombreMin) {
        this.nombreMin = nombreMin;
    }

    public Boolean getVigente() {
        return vigente;
    }

    public void setVigente(Boolean vigente) {
        this.vigente = vigente;
    }
}
