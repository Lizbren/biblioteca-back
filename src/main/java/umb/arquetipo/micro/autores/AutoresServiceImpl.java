package umb.arquetipo.micro.autores;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.AutoresDTO;

import java.util.ArrayList;
import java.util.List;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.tuples.TuplesAutor.*;

public class AutoresServiceImpl implements AutoresService {
    private static final String MICRO_SERVICE_NAME = "autores";

    Vertx vertx;
    JsonObject configuration;

    private static final String NUEVO_AUTOR = "INSERT INTO autores(nombre, fecha_nacimiento, pais, vigente) VALUES ($1, to_date($2,'YYYY-MM-DD'), $3, true) returning id_autor \"idAutor\"";
    private static final String AUTORES = "select id_autor \"idAutor\", nombre, fecha_nacimiento::text \"fechaNacimiento\", pais, vigente from autores order by nombre asc";
    private static final String ACTUALIZA_AUTOR = "UPDATE autores SET nombre = $2, fecha_nacimiento = to_date($3,'YYYY-MM-DD'), pais = $4 WHERE id_autor = $1 returning id_autor as \"idAutor\";";
    private static final String ACTUALIZA_VIGENCIA_AUTOR = "UPDATE autores SET vigente = $2 WHERE id_autor = $1;";
    private static final String EXISTE_AUTOR = "select exists(select * from autores where nombre =$1 and fecha_nacimiento = to_date($2,'YYYY-MM-DD') and pais = $3)";
    PgPool client;

    public AutoresServiceImpl(Vertx vertx, JsonObject configuration) {
        this.vertx = vertx;
        this.configuration = configuration;
        this.client = DataServiceConnect.open(this.vertx, this.configuration, MICRO_SERVICE_NAME);
    }

    @Override
    public Future<Integer> nuevoAutor(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_AUTOR)
                                        .execute(nuevoAutorTuple(argumentos))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(NUEVO_AUTOR)
                                                        .execute(nuevoAutorTuple(argumentos))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_AUTOR, row.getInteger(ID_AUTOR)));
                                                            return argumentos.getInteger(ID_AUTOR);
                                                        });
                                        })
                                        .compose(idAutor -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idAutor);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<List<AutoresDTO>> autores() {
        return this.client.preparedQuery(AUTORES)
                .execute()
                .map(rows -> {
                    List<AutoresDTO> autores = new ArrayList<>();
                    rows.forEach(row -> autores.add(new AutoresDTO(row.toJson())));
                    return autores;
                });
    }

    @Override
    public Future<Integer> actualizaAutor(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_AUTOR)
                                        .execute(nuevoAutorTuple(argumentos.getJsonObject(AUTOR)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(ACTUALIZA_AUTOR)
                                                        .execute(actualizaAutorTuple(argumentos.getJsonObject(AUTOR)))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_AUTOR, row.getInteger(ID_AUTOR)));
                                                            return argumentos.getInteger(ID_AUTOR);
                                                        });
                                        })
                                        .compose(idAutor -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idAutor);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> actualizaVariosAutores(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_AUTOR)
                .executeBatch(actualizaVariosAutoreTuple(argumentos))
                .map(rows -> 1);
    }

    @Override
    public Future<Integer> actualizaVigenciaAutores(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_VIGENCIA_AUTOR)
                .execute(actualizaVigenciaAutorTuple(argumentos))
                .map(rows -> 1);
    }
}
