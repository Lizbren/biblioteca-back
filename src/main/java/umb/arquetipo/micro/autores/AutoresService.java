package umb.arquetipo.micro.autores;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.AutoresDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface AutoresService {
    static AutoresService create(Vertx vertx, JsonObject configuration) {
        return new AutoresServiceImpl(vertx, configuration);
    }

    static AutoresService createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new AutoresServiceVertxEBProxy(vertx, address, options);
    }


    Future<Integer> nuevoAutor(JsonObject argumentos);

    Future<List<AutoresDTO>> autores();

    Future<Integer> actualizaAutor(JsonObject argumentos);

    Future<Integer> actualizaVariosAutores(JsonObject argumentos);

    Future<Integer> actualizaVigenciaAutores(JsonObject argumentos);
}
