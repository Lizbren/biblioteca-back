package umb.arquetipo.micro.prestamos;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.PrestamoDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface PrestamosService {
    static PrestamosService create(Vertx vertx, JsonObject configuration) {
        return new PrestamosServiceImpl(vertx, configuration);
    }

    static PrestamosService createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new PrestamosServiceVertxEBProxy(vertx, address, options);
    }

    Future<Integer> solicitarPrestamo(JsonObject argumentos);

    Future<List<PrestamoDTO>> prestamosPorUsuario(JsonObject argumentos);

    Future<List<PrestamoDTO>> prestamos();

    Future<Integer> autorizarPrestamo(JsonObject argumentos);

    Future<Integer> renovarPrestamo(JsonObject argumentos);

    Future<Integer> finalizarPrestamo(JsonObject argumentos);

    Future<Integer> actualizarDeudaPorUsuario(JsonObject argumentos);

    Future<Integer> actualizarDeudasUsuarios();

    Future<Integer> actualizarVencimientoSolicitudPorUsuario(JsonObject argumentos);

    Future<Integer> actualizarVencimientoSolicitudes();

    Future<Integer> noAutorizarPrestamo(JsonObject argumentos);
}
