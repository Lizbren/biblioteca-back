package umb.arquetipo.micro.prestamos;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Tuple;
import org.apache.log4j.Logger;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.PrestamoDTO;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.tuples.TuplePrestamos.prestamoTuple;

public class PrestamosServiceImpl implements PrestamosService {

    private static final String MICRO_SERVICE_NAME = "prestamos";
    PgPool client;
    private static final String SECRET_CODE = "7cVgr5cbdCZVw5WY";
    private static final String MODE_ALGORITHM = "AES";
    private static final String PRESTAMO_POR_USUARIO = "select  pr.id_prestamo \"idPrestamo\", pr.vencido,\n" +
            "pr.cuota::numeric, pr.autorizado, pr.fecha_prestamo::text \"fechaPrestamo\",pr.fecha_devolucion::text \"fechaDevolucion\", pr.fecha_limite \"fechaLimite\", pr.renovaciones, pr.fecha_apartado \"fechaApartado\",\n" +
            "li.titulo, li.isbn, li.paginas, li.edicion, e.nombre_may \"editorial\",\n" +
            " us.matricula, concat(us.nombres || ' ' || us.primer_apellido || ' ' || us.segundo_apellido) nombre,\n" +
            "concat(ad.nombres || ' ' || ad.primer_apellido || ' ' || ad.segundo_apellido) \"nombreAdministrador\",\n" +
            "tu.nombre_may \"tipoUsuario\", adi.semestre, ca.nombre_may \"carrera\"\n" +
            "from prestamos pr\n" +
            "inner join usuarios us\n" +
            "on us.id_usuario = pr.id_usuario\n" +
            "left join usuarios ad\n" +
            "on ad.id_usuario = pr.id_administrador\n" +
            "inner join tipo_usuarios tu\n" +
            "on tu.id_tipo_usuario = us.id_tipo_usuario\n" +
            "inner join libros li\n" +
            "on li.id_libro = pr.id_libro\n" +
            "inner join editorial e\n" +
            "on e.id_editorial = li.id_editorial\n" +
            "left join adicional adi\n" +
            "on adi.id_usuario = us.id_usuario\n" +
            "left join carrera ca\n" +
            "on ca.id_carrera = adi.id_carrera " +
            "where pr.id_usuario = $1 order by id_prestamo desc;";

    private static final String PRESTAMOS = "select  pr.id_prestamo \"idPrestamo\", pr.vencido,\n" +
            "pr.cuota::numeric, pr.autorizado, pr.fecha_prestamo::text \"fechaPrestamo\", pr.fecha_devolucion::text \"fechaDevolucion\",pr.fecha_limite \"fechaLimite\", pr.renovaciones, pr.fecha_apartado \"fechaApartado\",\n" +
            "li.titulo, li.isbn, li.paginas, li.edicion, e.nombre_may \"editorial\",\n" +
            " us.matricula, concat(us.nombres || ' ' || us.primer_apellido || ' ' || us.segundo_apellido) nombre,\n" +
            "concat(ad.nombres || ' ' || ad.primer_apellido || ' ' || ad.segundo_apellido) \"nombreAdministrador\",\n" +
            "tu.nombre_may \"tipoUsuario\", adi.semestre, ca.nombre_may \"carrera\"\n" +
            "from prestamos pr\n" +
            "inner join usuarios us\n" +
            "on us.id_usuario = pr.id_usuario\n" +
            "left join usuarios ad\n" +
            "on ad.id_usuario = pr.id_administrador\n" +
            "inner join tipo_usuarios tu\n" +
            "on tu.id_tipo_usuario = us.id_tipo_usuario\n" +
            "inner join libros li\n" +
            "on li.id_libro = pr.id_libro\n" +
            "inner join editorial e\n" +
            "on e.id_editorial = li.id_editorial\n" +
            "left join adicional adi\n" +
            "on adi.id_usuario = us.id_usuario\n" +
            "left join carrera ca\n" +
            "on ca.id_carrera = adi.id_carrera";

    private static final String AUTORIZAR = "update prestamos set fecha_prestamo = (select current_timestamp), autorizado = true, id_administrador = $1, fecha_limite = (select current_timestamp + '3 days') where id_prestamo = $2";

    private static final String ACTUALIZAR_DEUDA_POR_USUARIO = "update prestamos set cuota = \n" +
            "case when (((select current_timestamp) - fecha_limite) > '0 days 00:00:00'::interval) is true\n" +
            "then\n" +
            "\t(((select extract('days' from  (select current_timestamp) - fecha_limite)) + 1)::integer * 17)::money\n" +
            "else\n" +
            "\tcuota end\n" +
            "\twhere id_usuario = $1 and autorizado is true and vencido is false";
    private static final String ACTUALIZAR_DEUDAS_USUARIOS = "update prestamos set cuota = \n" +
            "case when (((select current_timestamp) - fecha_limite) > '0 days 00:00:00'::interval) is true\n" +
            "then\n" +
            "\t(((select extract('days' from  (select current_timestamp) - fecha_limite)) + 1)::integer * 17)::money\n" +
            "else\n" +
            "\tcuota end\n" +
            "\twhere autorizado is true and vencido is false";

    private static final String VENCIMIENTO_SOLICITUD_POR_USUARIO = "update prestamos set vencido = (select current_timestamp) > (fecha_apartado + '1 days') where autorizado is false and vencido is false and id_usuario = $1 returning id_libro \"idLibro\", vencido";
    private static final String VENCIMIENTO_SOLICITUDES = "update prestamos set vencido = (select current_timestamp) > (fecha_apartado + '1 days')  where autorizado is false and vencido is false returning id_libro \"idLibro\", vencido";


    PrestamosServiceImpl(Vertx vertx, JsonObject configuration) {
        this.client = DataServiceConnect.open(vertx, configuration, MICRO_SERVICE_NAME);
    }

    @Override
    public Future<Integer> solicitarPrestamo(JsonObject argumentos) {
        return this.client.preparedQuery("SELECT count(0) FROM PRESTAMOS where id_usuario = $1 and vencido is false")
                .execute(Tuple.of(argumentos.getJsonObject(SOLICITUD).getInteger(ID_USUARIO)))
                .map(rows -> {
                    rows.forEach(row -> argumentos.put(LIBRO, row.getInteger("count")));
                    return argumentos;
                })
                .compose(temp -> {
                    if (temp.getInteger(LIBRO) < 2) {
                        return this.client.preparedQuery("select exists(select * from prestamos where id_usuario = $1 and id_libro = $2 and vencido is false)")
                                .execute(Tuple.of(argumentos.getJsonObject(SOLICITUD).getInteger(ID_USUARIO), argumentos.getJsonObject(SOLICITUD).getInteger(ID_LIBRO)))
                                .map(rows -> {
                                    rows.forEach(row -> argumentos.put("exists", row.getBoolean("exists")));
                                    return argumentos;
                                }).compose(temp2 -> {
                                    if (Boolean.TRUE.equals(temp2.getBoolean("exists")))
                                        return Future.succeededFuture(-2);
                                    else
                                        return this.client.getConnection()
                                                .compose(conexion -> conexion.begin()
                                                        .compose(transaccion ->
                                                                conexion.preparedQuery("select disponibles, prestados, apartados from libros where id_libro = $1")
                                                                        .execute(Tuple.of(argumentos.getJsonObject(SOLICITUD).getInteger(ID_LIBRO)))
                                                                        .map(rows -> {
                                                                            rows.forEach(row -> argumentos.put("libro", row.toJson()));
                                                                            return argumentos;
                                                                        })
                                                                        .compose(nuevosArgumentos -> {
                                                                            if (nuevosArgumentos.getJsonObject(LIBRO).getInteger(DISPONIBLES) == 0) {
                                                                                return Future.failedFuture("No hay libros disponibles");
                                                                            } else {
                                                                                int nuevosDisponibles = nuevosArgumentos.getJsonObject(LIBRO).getInteger(DISPONIBLES) - 1;
                                                                                int nuevosApartados = nuevosArgumentos.getJsonObject(LIBRO).getInteger(APARTADOS) + 1;

                                                                                return conexion.preparedQuery("update libros set disponibles = $1, apartados = $2 where id_libro = $3")
                                                                                        .execute(Tuple.of(nuevosDisponibles, nuevosApartados, nuevosArgumentos.getJsonObject(SOLICITUD).getInteger(ID_LIBRO)))
                                                                                        .map(update -> nuevosArgumentos);
                                                                            }
                                                                        })
                                                                        .compose(nuevosArgumentos -> conexion.preparedQuery("INSERT INTO prestamos(id_usuario, id_libro, vencido, cuota, autorizado, renovaciones, fecha_apartado) VALUES ($1, $2, $3, $4::numeric::money, $5, $6, (select current_timestamp)) returning id_prestamo \"idPrestamo\"")
                                                                                .execute(prestamoTuple(nuevosArgumentos))
                                                                                .map(rows -> {
                                                                                    rows.forEach(row -> argumentos.put(ID_PRESTAMO, row.toJson().getInteger(ID_PRESTAMO)));
                                                                                    return argumentos.getInteger(ID_PRESTAMO);
                                                                                })
                                                                        )
                                                                        .compose(idPrestamo -> {
                                                                            transaccion.commit();
                                                                            return Future.succeededFuture(idPrestamo);
                                                                        }))
                                                        .eventually(v -> conexion.close()));
                                });
                    } else
                        return Future.succeededFuture(-1);
                });
    }

    @Override
    public Future<List<PrestamoDTO>> prestamosPorUsuario(JsonObject argumentos) {
        return this.client.preparedQuery(PRESTAMO_POR_USUARIO)
                .execute(Tuple.of(argumentos.getInteger(ID_USUARIO)))
                .compose(rows -> {
                    List<PrestamoDTO> prestamos = new ArrayList<>();
                    return Future.future(promise -> {
                        rows.forEach(row -> {
                            try {
                                prestamos.add(new PrestamoDTO(this.decodificar(row.toJson(), MATRICULA)));
                            } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException e) {
                                promise.fail(e);
                            }
                        });
                        promise.complete(prestamos);
                    });
                });
    }

    @Override
    public Future<List<PrestamoDTO>> prestamos() {
        return this.client.preparedQuery(PRESTAMOS)
                .execute()
                .compose(rows -> {
                    List<PrestamoDTO> prestamos = new ArrayList<>();
                    return Future.future(promise -> {
                        rows.forEach(row -> {
                            try {
                                prestamos.add(new PrestamoDTO(this.decodificar(row.toJson(), MATRICULA)));
                            } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException e) {
                                promise.fail(e);
                            }
                        });
                        promise.complete(prestamos);
                    });
                });
    }

    @Override
    public Future<Integer> autorizarPrestamo(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(AUTORIZAR)
                                        .execute(Tuple.of(argumentos.getInteger("idAdministrador"), argumentos.getInteger(ID_PRESTAMO)))
                                        .map(rows -> argumentos)
                                        .compose(nuevosArgumentos -> conexion.preparedQuery("update libros set prestados = prestados + 1, apartados = apartados - 1 where id_libro = (select id_libro from prestamos where id_prestamo = $1)")
                                                .execute(Tuple.of(nuevosArgumentos.getInteger(ID_PRESTAMO)))
                                                .map(update -> nuevosArgumentos.getInteger(ID_PRESTAMO)))
                                        .compose(idPrestamo -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idPrestamo);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    private static final Logger logger = Logger.getLogger(PrestamosServiceImpl.class);

    @Override
    public Future<Integer> renovarPrestamo(JsonObject argumentos) {
        logger.info(argumentos.getInteger(ID_PRESTAMO));
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery("select (select renovaciones from prestamos where id_prestamo = $1) = 3 renovacion")
                                        .execute(Tuple.of(argumentos.getInteger(ID_PRESTAMO)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put("renovacion", row.getBoolean("renovacion")));
                                            if (argumentos.getBoolean("renovacion").equals(true))
                                                return Future.failedFuture("Ya no tienes renovaciones para este prestamo");
                                            else
                                                return Future.succeededFuture(argumentos);
                                        })
                                        .compose(nuevosArgumentos -> {
                                                    logger.info(nuevosArgumentos);
                                                    return conexion.preparedQuery("update prestamos set \n" +
                                                                    "fecha_limite = \n" +
                                                                    "case when extract(dow from  (fecha_limite + '3 days')) = 6\n" +
                                                                    "\tthen ((select current_timestamp) + '5 days')\n" +
                                                                    "\telse\n" +
                                                                    "\t\tcase when extract(dow from  (fecha_limite + '3 days')) = 0\n" +
                                                                    "\t\tthen (fecha_limite + '4 days')\n" +
                                                                    "\t\telse (fecha_limite + '3 days') end\n" +
                                                                    "\tend,\n" +
                                                                    "renovaciones = renovaciones + 1\n" +
                                                                    "where id_prestamo = $1")
                                                            .execute(Tuple.of(nuevosArgumentos.getInteger(ID_PRESTAMO)))
                                                            .map(update -> nuevosArgumentos.getInteger(ID_PRESTAMO));
                                                }
                                        ).compose(idPrestamo -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idPrestamo);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> finalizarPrestamo(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery("update prestamos set vencido = true, fecha_devolucion = (select current_timestamp ) where id_prestamo = $1")
                                        .execute(Tuple.of(argumentos.getInteger(ID_PRESTAMO)))
                                        .map(rows -> argumentos)
                                        .compose(nuevosArgumentos -> conexion.preparedQuery("update libros set disponibles = disponibles + 1, prestados = prestados - 1 where id_libro = (select id_libro from prestamos where id_prestamo = $1)")
                                                .execute(Tuple.of(nuevosArgumentos.getInteger(ID_PRESTAMO)))
                                                .map(rows -> nuevosArgumentos.getInteger(ID_PRESTAMO)))
                                        .compose(idPrestamo -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idPrestamo);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> actualizarDeudaPorUsuario(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZAR_DEUDA_POR_USUARIO)
                .execute(Tuple.of(argumentos.getInteger(ID_USUARIO)))
                .map(rows -> argumentos.getInteger(ID_USUARIO));
    }

    @Override
    public Future<Integer> actualizarDeudasUsuarios() {
        return this.client.preparedQuery(ACTUALIZAR_DEUDAS_USUARIOS)
                .execute()
                .map(rows -> 1);
    }

    @Override
    public Future<Integer> actualizarVencimientoSolicitudPorUsuario(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                this.client.preparedQuery(VENCIMIENTO_SOLICITUD_POR_USUARIO)
                                        .execute(Tuple.of(argumentos.getInteger(ID_USUARIO)))
                                        .map(rows -> {
                                            List<Tuple> listaIdLibros = new ArrayList<>();
                                            if (rows.rowCount() > 0)
                                                rows.forEach(row -> listaIdLibros.add(Tuple.of(row.getInteger(ID_LIBRO), row.getBoolean("vencido"))));
                                            return listaIdLibros;
                                        })
                                        .compose(idLibro -> {
                                            if (!idLibro.isEmpty())
                                                return conexion.preparedQuery("update libros set disponibles = disponibles + 1, apartados = apartados - 1 where id_libro = $1 and $2")
                                                        .executeBatch(idLibro)
                                                        .map(f -> 1);
                                            else
                                                return Future.succeededFuture(1);
                                        })
                                        .compose(idPrestamo -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idPrestamo);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> actualizarVencimientoSolicitudes() {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(VENCIMIENTO_SOLICITUDES)
                                        .execute()
                                        .map(rows -> {
                                            List<Tuple> listaIdLibros = new ArrayList<>();
                                            rows.forEach(row -> listaIdLibros.add(Tuple.of(row.getInteger(ID_LIBRO), row.getBoolean("vencido"))));
                                            return listaIdLibros;
                                        })
                                        .compose(batchIdLibro -> {
                                            if (!batchIdLibro.isEmpty())
                                                return conexion.preparedQuery("update libros set disponibles = disponibles + 1, apartados = apartados - 1 where id_libro = $1 and $2")
                                                        .executeBatch(batchIdLibro)
                                                        .map(f -> 1);
                                            else
                                                return Future.succeededFuture(1);
                                        })
                                        .compose(idPrestamo -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(1);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> noAutorizarPrestamo(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery("update prestamos set vencido = true where id_prestamo = $1")
                                        .execute(Tuple.of(argumentos.getInteger(ID_PRESTAMO)))
                                        .map(rows -> argumentos)
                                        .compose(nuevosArgumentos -> conexion.preparedQuery("update libros set disponibles = disponibles + 1, apartados = apartados - 1 where id_libro = (select id_libro from prestamos where id_prestamo = $1)")
                                                .execute(Tuple.of(nuevosArgumentos.getInteger(ID_PRESTAMO)))
                                                .map(rows -> nuevosArgumentos.getInteger(ID_PRESTAMO)))
                                        .compose(idPrestamo -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idPrestamo);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    private JsonObject decodificar(JsonObject argumentos, String llave) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        byte[] encryptedBytes = Base64.getDecoder().decode(argumentos.getString(llave).replace("\n", ""));
        Key aesKey = new SecretKeySpec(SECRET_CODE.getBytes(), MODE_ALGORITHM);
        Cipher cipher = Cipher.getInstance(MODE_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        argumentos.put(llave, new String(cipher.doFinal(encryptedBytes)));
        return argumentos;
    }
}
