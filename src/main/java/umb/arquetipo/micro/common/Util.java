package umb.arquetipo.micro.common;

public class Util {
    // LLaves para el micro servicio de autores
    public static final String NOMBRE_AUTOR = "nombre";
    public static final String FECHA_NAC = "fechaNacimiento";
    public static final String PAIS = "pais";
    public static final String ID_AUTOR = "idAutor";
    public static final String AUTORES = "autores";
    public static final String AUTOR = "autor";
    public static final String MATRICULA = "matricula";
    public static final String CLAVE = "clave";
    public static final String USUARIO = "usuario";
    public static final String NOMBRES = "nombres";
    public static final String PRIMER_APELLIDO = "primerApellido";
    public static final String SEGUNDO_APELLIDO = "segundoApellido";
    public static final String VIGENCIA = "vigencia";
    public static final String CORREO = "correo";
    public static final String TIPO_USUARIO = "idTipoUsuario";
    public static final String FECHA_ALTA = "fechaAlta";
    public static final String CICLO_ACTUAL = "cicloActual";
    public static final String ID_USUARIO = "idUsuario";
    public static final String EXISTE = "existe";
    // Llaves para el micro servicio de catalogos comunes
    public static final String CATEGORIA = "categoria";
    public static final String CATEGORIAS = "categorias";
    public static final String ID_CATEGORIA = "idCategoria";
    public static final String EDITORIAL = "editorial";
    public static final String EDITORIALES = "editoriales";
    public static final String ID_EDITORIAL = "idEditorial";
    public static final String CARRERA = "carrera";
    public static final String CARRERAS = "carreras";
    public static final String ID_CARRERA = "idCarrera";
    public static final String NOMBRE_MAY = "nombreMay";
    public static final String NOMBRE_MIN = "nombreMin";
    public static final String VIGENTE = "vigente";

    public static final String ID_LIBRO = "idLibro";
    public static final String LIBRO = "libro";
    public static final String TITULO = "titulo";
    public static final String ISBN = "isbn";
    public static final String PAGINAS = "paginas";
    public static final String EXISTENCIA = "existencia";
    public static final String EDICION = "edicion";
    public static final String DISPONIBLES = "disponibles";
    public static final String PRESTADOS = "prestados";
    public static final String APARTADOS = "apartados";
    public static final String SOLICITUD = "solicitud";
    public static final String ID_PRESTAMO = "idPrestamo";
    public static final String ADICIONAL = "adicional";
    public static final String EXISTS = "exists";


    Util() {
    }

}
