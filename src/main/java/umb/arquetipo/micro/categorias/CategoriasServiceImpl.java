package umb.arquetipo.micro.categorias;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Tuple;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.CategoriasDTO;

import java.util.ArrayList;
import java.util.List;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.tuples.TuplesCategorias.*;

public class CategoriasServiceImpl implements CategoriasService {
    private static final String MICRO_SERVICE_NAME = "categorias";

    Vertx vertx;
    JsonObject configuration;
    PgPool client;
    private static final String NUEVA_CATEGORIA = "INSERT INTO categorias(nombre_may, nombre_min, vigente) VALUES ($1, $2, $3) returning id_categoria;";
    private static final String CATEGORIAS = "select id_categoria \"idCategoria\" ,nombre_may \"nombreMay\", nombre_min \"nombreMin\", vigente from categorias order by nombre_may asc";
    private static final String ACTUALIZA_CATEGORIA = "UPDATE categorias SET nombre_may = $2, nombre_min = $3 WHERE id_categoria = $1 returning id_categoria \"idCategoria\";";
    private static final String ACTUALIZA_VIGENCIA_CATEGORIA = "UPDATE categorias SET  vigente = $2 WHERE id_categoria = $1;";
    private static final String EXISTE_CATEGORIA = "select exists(select * from categorias where nombre_may = $1)";


    public CategoriasServiceImpl(Vertx vertx, JsonObject configuration) {
        this.vertx = vertx;
        this.configuration = configuration;
        this.client = DataServiceConnect.open(this.vertx, this.configuration, MICRO_SERVICE_NAME);
    }

    @Override
    public Future<Integer> nuevaCategoria(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_CATEGORIA)
                                        .execute(Tuple.of(argumentos.getJsonObject(CATEGORIA).getString(NOMBRE_MAY)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(NUEVA_CATEGORIA)
                                                        .execute(nuevaCategoriaTuple(argumentos))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_CATEGORIA, row.getInteger("id_categoria")));
                                                            return argumentos.getInteger(ID_CATEGORIA);
                                                        });
                                        })
                                        .compose(idCategoria -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idCategoria);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<List<CategoriasDTO>> categorias() {
        return this.client.preparedQuery(CATEGORIAS)
                .execute()
                .map(rows -> {
                    List<CategoriasDTO> categorias = new ArrayList<>();
                    rows.forEach(row -> categorias.add(new CategoriasDTO(row.toJson())));
                    return categorias;
                });
    }

    @Override
    public Future<Integer> actualizaCategoria(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_CATEGORIA)
                                        .execute(Tuple.of(argumentos.getJsonObject(CATEGORIA).getString(NOMBRE_MAY)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(ACTUALIZA_CATEGORIA)
                                                        .execute(actualizaCategoriaTuple(argumentos.getJsonObject(CATEGORIA)))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_CATEGORIA, row.toJson().getInteger(ID_CATEGORIA)));
                                                            return argumentos.getInteger(ID_CATEGORIA);
                                                        });
                                        })
                                        .compose(idCategoria -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idCategoria);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> actualizaVariasCategorias(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_CATEGORIA)
                .executeBatch(actualizaVariasCategoriasTuple(argumentos))
                .map(rows -> 1);
    }

    @Override
    public Future<Integer> actualizaVigenciaCategoria(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_VIGENCIA_CATEGORIA)
                .execute(actualizaVigenciaCategoriaTuple(argumentos))
                .map(rows -> 1);
    }
}
