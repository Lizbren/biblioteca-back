package umb.arquetipo.micro.categorias;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.CategoriasDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface CategoriasService {
    static CategoriasService create(Vertx vertx, JsonObject configuration) {
        return new CategoriasServiceImpl(vertx, configuration);
    }

    static CategoriasService createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new CategoriasServiceVertxEBProxy(vertx, address, options);
    }

    Future<Integer> nuevaCategoria(JsonObject argumentos);

    Future<List<CategoriasDTO>> categorias();

    Future<Integer> actualizaCategoria(JsonObject argumentos);

    Future<Integer> actualizaVariasCategorias(JsonObject argumentos);
    Future<Integer> actualizaVigenciaCategoria(JsonObject argumentos);
}
