package umb.arquetipo.micro.carrera;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.CarreraDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface CarreraService {
    static CarreraService create(Vertx vertx, JsonObject configuration) {
        return new CarreraServiceImpl(vertx, configuration);
    }

    static CarreraService createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new CarreraServiceVertxEBProxy(vertx, address, options);
    }

    Future<Integer> nuevaCarrera(JsonObject argumentos);

    Future<List<CarreraDTO>> carreras();

    Future<Integer> actualizaCarrera(JsonObject argumentos);

    Future<Integer> actualizaVariasCarreras(JsonObject argumentos);

    Future<Integer> actualizaVigenciaCarrera(JsonObject argumentos);
}
