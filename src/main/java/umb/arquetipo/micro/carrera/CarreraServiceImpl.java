package umb.arquetipo.micro.carrera;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Tuple;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.CarreraDTO;

import java.util.ArrayList;
import java.util.List;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.tuples.TuplesCarrera.*;

public class CarreraServiceImpl implements CarreraService {
    private static final String MICRO_SERVICE_NAME = "carreras";
    PgPool client;
    private static final String NUEVA_CARRERA = "INSERT INTO carrera(nombre_may, nombre_min, vigente) VALUES ($1, $2, $3) returning id_carrera as \"idCarrera\";";
    private static final String CARRERAS = "select id_carrera \"idCarrera\" ,nombre_may \"nombreMay\", nombre_min \"nombreMin\", vigente from carrera order by nombre_may asc";
    private static final String ACTUALIZA_CARRERA = "UPDATE carrera SET nombre_may = $2, nombre_min = $3 WHERE id_carrera = $1 returning id_carrera as \"idCarrera\";";
    private static final String ACTUALIZA_VIGENCIA_CARRERA = "UPDATE carrera SET  vigente = $2 WHERE id_carrera = $1;";
    private static final String EXISTE_CARRERA = "select exists(select * from carrera where nombre_may = $1)";

    public CarreraServiceImpl(Vertx vertx, JsonObject configuration) {
        this.client = DataServiceConnect.open(vertx, configuration, MICRO_SERVICE_NAME);
    }

    @Override
    public Future<Integer> nuevaCarrera(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_CARRERA)
                                        .execute(Tuple.of(argumentos.getJsonObject(CARRERA).getString(NOMBRE_MAY)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(NUEVA_CARRERA)
                                                        .execute(nuevaCarreraTuple(argumentos))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_CARRERA, row.toJson().getInteger(ID_CARRERA)));
                                                            return argumentos.getInteger(ID_CARRERA);
                                                        });
                                        })
                                        .compose(idCarrera -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idCarrera);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<List<CarreraDTO>> carreras() {
        return this.client.preparedQuery(CARRERAS)
                .execute()
                .map(rows -> {
                    List<CarreraDTO> categorias = new ArrayList<>();
                    rows.forEach(row -> categorias.add(new CarreraDTO(row.toJson())));
                    return categorias;
                });
    }

    @Override
    public Future<Integer> actualizaCarrera(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_CARRERA)
                                        .execute(Tuple.of(argumentos.getJsonObject(CARRERA).getString(NOMBRE_MAY)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(ACTUALIZA_CARRERA)
                                                        .execute(actualizaCarreraTuple(argumentos.getJsonObject(CARRERA)))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_CARRERA, row.toJson().getInteger(ID_CARRERA)));
                                                            return argumentos.getInteger(ID_CARRERA);
                                                        });
                                        })
                                        .compose(idCarrera -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idCarrera);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> actualizaVariasCarreras(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_CARRERA)
                .executeBatch(actualizaVariasCarrerasTuple(argumentos))
                .map(rows -> 1);
    }

    @Override
    public Future<Integer> actualizaVigenciaCarrera(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_VIGENCIA_CARRERA)
                .execute(actualizaVigenciaCarreraTuple(argumentos))
                .map(rows -> 1);
    }
}
