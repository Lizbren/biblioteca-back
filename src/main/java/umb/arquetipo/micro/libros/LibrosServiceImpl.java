package umb.arquetipo.micro.libros;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.SqlConnection;
import io.vertx.sqlclient.Tuple;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.LibrosDTO;

import java.util.ArrayList;
import java.util.List;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.tuples.TuplesLibro.*;

public class LibrosServiceImpl implements LibrosService {
    private static final String MICRO_SERVICE_NAME = "libros";
    PgPool client;
    private static final String NUEVO_LIBRO = "INSERT INTO libros(titulo, isbn, paginas, existencia, edicion, id_editorial, disponibles, prestados, apartados) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9) returning id_libro as \"idLibro\"";
    private static final String RELACION_AUTOR = "INSERT INTO autor(id_libro, id_autor) VALUES ($1, $2)";
    private static final String RELACION_CATEGORIA = "INSERT INTO categoria(id_libro, id_categoria)VALUES ($1, $2)";
    private static final String LIBROS = "select id_libro \"idLibro\", nombre_may editorial, edi.id_editorial \"idEditorial\", titulo, isbn, paginas, existencia, edicion, disponibles, prestados, apartados,\n" +
            "       (SELECT array_to_json(array_agg(auts.nombre)) from autor aut\n" +
            "                                                              inner join autores auts on\n" +
            "               auts.id_autor = aut.id_autor\n" +
            "        where id_libro = li.id_libro) autores,\n" +
            "       (SELECT array_to_json(array_agg(auts.id_autor)) from autor aut\n" +
            "                                                                inner join autores auts on\n" +
            "               auts.id_autor = aut.id_autor\n" +
            "        where id_libro = li.id_libro) \"idAutores\",\n" +
            "       (SELECT array_to_json(array_agg(cats.nombre_may)) from categoria cat\n" +
            "                                                                  inner join categorias cats on\n" +
            "               cats.id_categoria = cat.id_categoria\n" +
            "        where id_libro = li.id_libro) categorias,\n" +
            "       (SELECT array_to_json(array_agg(cats.id_categoria)) from categoria cat\n" +
            "                                                                    inner join categorias cats on\n" +
            "               cats.id_categoria = cat.id_categoria\n" +
            "        where id_libro = li.id_libro) \"idCategorias\"" +
            "\n" +
            "from libros li\n" +
            "inner join editorial edi\n" +
            "                    on edi.id_editorial = li.id_editorial order by titulo asc";

    private static final String ACTUALIZA_LIBRO = "UPDATE libros SET titulo=$2, isbn=$3, paginas=$4, existencia=$5, edicion=$6, id_editorial=$7, disponibles=$8, prestados=$9, apartados=$10 WHERE id_libro = $1;";
    private static final String DELETE_AUTORES = "delete from autor where id_libro = $1";
    private static final String DELETE_CATEGORIAS = "delete from categoria where id_libro= $1";
    private static final String EXISTE_LIBRO_BY_ID = "select exists(select * from libros where isbn =$1 and id_libro != $2)";
    private static final String EXISTE_LIBRO = "select exists(select * from libros where isbn =$1)";


    LibrosServiceImpl(Vertx vertx, JsonObject configuration) {
        this.client = DataServiceConnect.open(vertx, configuration, MICRO_SERVICE_NAME);
    }

    private static final Logger logger = LoggerFactory.getLogger(LibrosServiceImpl.class);

    @Override
    public Future<Integer> nuevoLibro(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_LIBRO)
                                        .execute(Tuple.of(argumentos.getJsonObject(LIBRO).getString(ISBN)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(NUEVO_LIBRO)
                                                        .execute(nuevoLibroTuple(argumentos.getJsonObject(LIBRO)))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_LIBRO, row.getInteger(ID_LIBRO)));
                                                            return argumentos;
                                                        })
                                                        .compose(nuevosArgumentos -> this.relacionAutores(nuevosArgumentos, conexion))
                                                        .compose(nuevosArgumentos -> this.relacionCategoria(nuevosArgumentos, conexion))
                                                        .map(newArguments -> newArguments.getInteger(ID_LIBRO));
                                        })
                                        .compose(idLibro -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idLibro);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<List<LibrosDTO>> libros() {
        return this.client.preparedQuery(LIBROS)
                .execute()
                .map(rows -> {
                    List<LibrosDTO> libros = new ArrayList<>();
                    rows.forEach(row -> libros.add(new LibrosDTO(row.toJson())));
                    return libros;
                });
    }

    @Override
    public Future<Integer> actualizaLibro(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_LIBRO_BY_ID)
                                        .execute(Tuple.of(argumentos.getJsonObject(LIBRO).getString(ISBN), argumentos.getInteger(ID_LIBRO)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(ACTUALIZA_LIBRO)
                                                        .execute(actualizaLibroTuple(argumentos))
                                                        .compose(result -> this.eliminarAutores(argumentos, conexion))
                                                        .compose(nuevosArgumentos -> this.eliminarCategorias(nuevosArgumentos, conexion))
                                                        .compose(nuevosArgumentos -> this.relacionAutores(nuevosArgumentos, conexion))
                                                        .compose(nuevosArgumentos -> this.relacionCategoria(nuevosArgumentos, conexion))
                                                        .map(newArguments -> newArguments.getInteger(ID_LIBRO));
                                        })
                                        .compose(idLibro -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idLibro);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    private Future<JsonObject> eliminarAutores(JsonObject argumentos, SqlConnection conexion) {
        return conexion.preparedQuery(DELETE_AUTORES)
                .execute(idLibroTuple(argumentos))
                .map(result -> argumentos);
    }

    private Future<JsonObject> eliminarCategorias(JsonObject argumentos, SqlConnection conexion) {
        return conexion.preparedQuery(DELETE_CATEGORIAS)
                .execute(idLibroTuple(argumentos))
                .map(result -> argumentos);
    }

    private Future<JsonObject> relacionAutores(JsonObject argumentos, SqlConnection conexion) {
        return conexion.preparedQuery(RELACION_AUTOR)
                .executeBatch(relacionAutorTuple(argumentos))
                .map(result -> argumentos);
    }

    private Future<JsonObject> relacionCategoria(JsonObject argumentos, SqlConnection conexion) {
        return conexion.preparedQuery(RELACION_CATEGORIA)
                .executeBatch(relacionCategoriaTuple(argumentos))
                .map(result -> argumentos);
    }
}
