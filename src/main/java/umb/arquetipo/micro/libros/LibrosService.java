package umb.arquetipo.micro.libros;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.LibrosDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface LibrosService {
    static LibrosService create(Vertx vertx, JsonObject configuration) {
        return new LibrosServiceImpl(vertx, configuration);
    }

    static LibrosService createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new LibrosServiceVertxEBProxy(vertx, address, options);
    }

    Future<Integer> nuevoLibro(JsonObject argumentos);

    Future<List<LibrosDTO>> libros();

    Future<Integer> actualizaLibro(JsonObject argumentos);
}
