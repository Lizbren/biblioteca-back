package umb.arquetipo.micro.editoriales;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Tuple;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.EditorialesDTO;

import java.util.ArrayList;
import java.util.List;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.tuples.TuplesCategorias.actualizaCategoriaTuple;
import static umb.arquetipo.micro.tuples.TuplesCategorias.nuevaCategoriaTuple;
import static umb.arquetipo.micro.tuples.TuplesEditorial.*;

public class EditorialesServiceImpl implements EditorialesService {
    private static final String MICRO_SERVICE_NAME = "editoriales";

    Vertx vertx;
    JsonObject configuration;
    PgPool client;
    private static final String NUEVA_EDITORIAL = "INSERT INTO editorial(nombre_may, nombre_min, vigente) VALUES ($1, $2, $3) returning id_editorial \"idEditorial\";";
    private static final String EDITORIALES = "select id_editorial \"idEditorial\" ,nombre_may \"nombreMay\", nombre_min \"nombreMin\", vigente from editorial order by nombre_may asc";
    private static final String ACTUALIZA_EDITORIAL = "UPDATE editorial SET nombre_may = $2, nombre_min = $3 WHERE id_editorial = $1 returning id_editorial \"idEditorial\";";
    private static final String ACTUALIZA_VIGENCIA_EDITORIAL = "UPDATE editorial SET  vigente = $2 WHERE id_editorial = $1;";
    private static final String EXISTE_EDITORIAL = "select exists(select * from editorial where nombre_may = $1)";


    EditorialesServiceImpl(Vertx vertx, JsonObject configuration) {
        this.vertx = vertx;
        this.configuration = configuration;
        this.client = DataServiceConnect.open(this.vertx, this.configuration, MICRO_SERVICE_NAME);
    }

    @Override
    public Future<Integer> nuevaEditorial(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_EDITORIAL)
                                        .execute(Tuple.of(argumentos.getJsonObject(EDITORIAL).getString(NOMBRE_MAY)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(NUEVA_EDITORIAL)
                                                        .execute(nuevaEditorialTuple(argumentos))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_EDITORIAL, row.getInteger(ID_EDITORIAL)));
                                                            return argumentos.getInteger(ID_EDITORIAL);
                                                        });
                                        })
                                        .compose(idEditorial -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idEditorial);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<List<EditorialesDTO>> editoriales() {
        return this.client.preparedQuery(EDITORIALES)
                .execute()
                .map(rows -> {
                    List<EditorialesDTO> editoriales = new ArrayList<>();
                    rows.forEach(row -> editoriales.add(new EditorialesDTO(row.toJson())));
                    return editoriales;
                });
    }

    @Override
    public Future<Integer> actualizaEditorial(JsonObject argumentos) {
        return this.client.getConnection()
                .compose(conexion -> conexion.begin()
                        .compose(transaccion ->
                                conexion.preparedQuery(EXISTE_EDITORIAL)
                                        .execute(Tuple.of(argumentos.getJsonObject(EDITORIAL).getString(NOMBRE_MAY)))
                                        .compose(rows -> {
                                            rows.forEach(row -> argumentos.put(EXISTS, row.getBoolean(EXISTS)));
                                            if (argumentos.getBoolean(EXISTS).equals(true))
                                                return Future.succeededFuture(-1);
                                            else
                                                return conexion.preparedQuery(ACTUALIZA_EDITORIAL)
                                                        .execute(actualizaEditorialTuple(argumentos.getJsonObject(EDITORIAL)))
                                                        .map(resultados -> {
                                                            resultados.forEach(row -> argumentos.put(ID_EDITORIAL, row.getInteger(ID_EDITORIAL)));
                                                            return argumentos.getInteger(ID_EDITORIAL);
                                                        });
                                        })
                                        .compose(idEditorial -> {
                                            transaccion.commit();
                                            return Future.succeededFuture(idEditorial);
                                        }))
                        .eventually(v -> conexion.close()));
    }

    @Override
    public Future<Integer> actualizaVariasEditoriales(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_EDITORIAL)
                .executeBatch(actualizaVariasEditorialTuple(argumentos))
                .map(rows -> 1);
    }

    @Override
    public Future<Integer> actualizaVigenciaEditoriales(JsonObject argumentos) {
        return this.client.preparedQuery(ACTUALIZA_VIGENCIA_EDITORIAL)
                .execute(actualizaVigenciaEditorialTuple(argumentos))
                .map(rows -> 1);
    }
}
