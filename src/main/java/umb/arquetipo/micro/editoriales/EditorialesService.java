package umb.arquetipo.micro.editoriales;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.AutoresDTO;
import umb.arquetipo.micro.dto.EditorialesDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface EditorialesService {
    static EditorialesService create(Vertx vertx, JsonObject configuration) {
        return new EditorialesServiceImpl(vertx, configuration);
    }

    static EditorialesService createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new EditorialesServiceVertxEBProxy(vertx, address, options);
    }


    Future<Integer> nuevaEditorial(JsonObject argumentos);

    Future<List<EditorialesDTO>> editoriales();

    Future<Integer> actualizaEditorial(JsonObject argumentos);

    Future<Integer> actualizaVariasEditoriales(JsonObject argumentos);

    Future<Integer> actualizaVigenciaEditoriales(JsonObject argumentos);
}
