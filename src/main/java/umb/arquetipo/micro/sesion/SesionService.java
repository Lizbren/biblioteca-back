package umb.arquetipo.micro.sesion;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import umb.arquetipo.micro.dto.SesionDTO;

import java.util.List;

@ProxyGen
@VertxGen
public interface SesionService {
    static SesionService create(Vertx vertx, JsonObject configuration) {
        return new SesionServiceImpl(vertx, configuration);
    }

    static SesionService createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new SesionServiceVertxEBProxy(vertx, address, options);
    }

    Future<SesionDTO> loginUsuario(JsonObject argumentos);

    Future<Integer> nuevoUsuario(JsonObject argumentos);

    Future<Integer> actualizarPass(JsonObject argumentos);

    Future<List<SesionDTO>> usuarios();
}
