package umb.arquetipo.micro.sesion;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Tuple;
import umb.arquetipo.connect.DataServiceConnect;
import umb.arquetipo.micro.dto.SesionDTO;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.tuples.TuplesUsuarios.loginUsuarioTuple;
import static umb.arquetipo.micro.tuples.TuplesUsuarios.nuevoUsuarioTuple;

public class SesionServiceImpl implements SesionService {
    private static final String MICRO_SERVICE_NAME = "SESION";
    private static final Logger logger = LoggerFactory.getLogger(SesionServiceImpl.class);

    Vertx vertx;
    JsonObject configuration;
    PgPool client;
    private static final String LOGIN = "select us.id_usuario \"idUsuario\", nombres, primer_apellido \"primerApellido\", segundo_apellido \"segundoApellido\", matricula, vigencia, correo, contrasena, tu.id_tipo_usuario \"idTipoUsuario\", tu.nombre_may \"tipoUsuario\",fecha_alta \"fechaAlta\", ciclo_actual \"cicloActual\", permisos, ad.semestre, ca.nombre_may \"carrera\"  from usuarios us\n" +
            "inner join tipo_usuarios tu\n" +
            "on us.id_tipo_usuario = tu.id_tipo_usuario\n" +
            "left join adicional ad\n" +
            "on ad.id_usuario = us.id_usuario\n" +
            "left join carrera ca\n" +
            "on ca.id_carrera = ad.id_carrera\n" +
            "where matricula = $1 and contrasena = $2";
    private static final String USUARIOS = "select us.id_usuario \"idUsuario\", nombres, primer_apellido \"primerApellido\", segundo_apellido \"segundoApellido\", matricula, vigencia, correo, contrasena, tu.id_tipo_usuario \"idTipoUsuario\", tu.nombre_may \"tipoUsuario\",fecha_alta \"fechaAlta\", ciclo_actual \"cicloActual\", permisos, ad.semestre, ca.nombre_may \"carrera\"  from usuarios us\n" +
            "inner join tipo_usuarios tu\n" +
            "on us.id_tipo_usuario = tu.id_tipo_usuario\n" +
            "left join adicional ad\n" +
            "on ad.id_usuario = us.id_usuario\n" +
            "left join carrera ca\n" +
            "on ca.id_carrera = ad.id_carrera\n";
    private static final String NUEVO_USUARIO = "INSERT INTO usuarios(nombres, primer_apellido, segundo_apellido, matricula, vigencia, correo, contrasena, id_tipo_usuario, fecha_alta, ciclo_actual) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, (select current_date), $9) returning id_usuario \"idUsuario\"";
    private static final String SECRET_CODE = "7cVgr5cbdCZVw5WY";
    private static final String MODE_ALGORITHM = "AES";
    private static final String EXISTE_MATRICULA = "select exists (select matricula from usuarios where matricula = $1) existe";

    SesionServiceImpl(Vertx vertx, JsonObject configuration) {
        this.vertx = vertx;
        this.configuration = configuration;
        this.client = DataServiceConnect.open(this.vertx, this.configuration, MICRO_SERVICE_NAME);
    }

    @Override
    public Future<SesionDTO> loginUsuario(JsonObject argumentos) {
        return this.codificar(argumentos, CLAVE)
                .compose(newArguments -> this.codificar(argumentos, MATRICULA))
                .compose(nuevosArgumentos -> this.client.preparedQuery(LOGIN)
                        .execute(loginUsuarioTuple(argumentos.getJsonObject(USUARIO)))
                        .map(rows -> {
                            if (rows.rowCount() == 1)
                                rows.forEach(row -> argumentos.put(USUARIO, row.toJson()));

                            return argumentos;
                        }))
                .compose(nuevosArgumentos -> {
                    if (nuevosArgumentos.getJsonObject(USUARIO).containsKey(ID_USUARIO))
                        return Future.succeededFuture(argumentos);
                    else
                        return Future.failedFuture("USUARIO NO ENCONTRADO");
                })
                .compose(nuevosArgumentos -> this.decodificar(argumentos, MATRICULA))
                .compose(nuevosArgumentos -> this.decodificar(argumentos, CORREO))
                .compose(nuevosArgumentos -> Future.succeededFuture(new SesionDTO(nuevosArgumentos.getJsonObject(USUARIO))));
    }

    @Override
    public Future<Integer> nuevoUsuario(JsonObject argumentos) {
        return this.codificar(argumentos, CLAVE)
                .compose(nuevosArgumentos -> this.codificar(nuevosArgumentos, MATRICULA))
                .compose(nuevosArgumentos -> this.codificar(nuevosArgumentos, CORREO))
                .compose(nuevosArgumentos -> this.client.preparedQuery(EXISTE_MATRICULA)
                        .execute(Tuple.of(argumentos.getJsonObject(USUARIO).getString(MATRICULA)))
                        .compose(exist -> {
                            exist.forEach(row -> argumentos.put(EXISTE, row.getBoolean(EXISTE)));
                            if (Boolean.TRUE.equals(argumentos.getBoolean(EXISTE)))
                                return Future.succeededFuture(-1);
                            else
                                return this.client.preparedQuery(NUEVO_USUARIO)
                                        .execute(nuevoUsuarioTuple(nuevosArgumentos.getJsonObject(USUARIO)))
                                        .map(rows -> {
                                            rows.forEach(row -> nuevosArgumentos.put(ID_USUARIO, row.toJson().getInteger(ID_USUARIO)));
                                            return nuevosArgumentos;
                                        }).compose(nuevosArgumentosTemp -> {
                                            if (nuevosArgumentosTemp.getJsonObject(USUARIO).containsKey(ADICIONAL)) {
                                                return this.agregarAdicional(nuevosArgumentosTemp);
                                            } else
                                                return Future.succeededFuture(nuevosArgumentosTemp.getInteger(ID_USUARIO));
                                        });
                        }));
    }

    @Override
    public Future<Integer> actualizarPass(JsonObject argumentos) {
        argumentos.put(USUARIO, new JsonObject());
        argumentos.getJsonObject(USUARIO).put(CLAVE, argumentos.getString(CLAVE));
        argumentos.getJsonObject(USUARIO).put("claveAnterior", argumentos.getString("claveAnterior"));
        return this.codificar(argumentos, CLAVE)
                .compose(nuevosArgumentos -> this.codificar(nuevosArgumentos, "claveAnterior"))
                .compose(nuevosArgumentos -> this.client.preparedQuery("select exists(select * from usuarios where id_usuario =$1 and contrasena = $2) as existe ")
                        .execute(Tuple.of(nuevosArgumentos.getInteger(ID_USUARIO), nuevosArgumentos.getJsonObject(USUARIO).getString("claveAnterior")))
                        .compose(rows -> {
                            rows.forEach(row -> argumentos.put(EXISTE, row.getBoolean(EXISTE)));
                            if (Boolean.FALSE.equals(argumentos.getBoolean(EXISTE)))
                                return Future.succeededFuture(-1);
                            else
                                return this.client.preparedQuery("update usuarios set contrasena =$1 where id_usuario = $2")
                                        .execute(Tuple.of(nuevosArgumentos.getJsonObject(USUARIO).getString(CLAVE), nuevosArgumentos.getInteger(ID_USUARIO)))
                                        .map(r -> nuevosArgumentos.getInteger(ID_USUARIO));
                        })
                );
    }

    @Override
    public Future<List<SesionDTO>> usuarios() {
        return this.client.preparedQuery(USUARIOS)
                .execute()
                .map(rows -> {
                    List<SesionDTO> usuarios = new ArrayList<>();
                    rows.forEach(row -> usuarios.add(new SesionDTO(row.toJson())));
                    return usuarios.stream()
                            .map(user -> {
                                JsonObject test = new JsonObject();
                                try {
                                    test = this.decode(user.toJson(), MATRICULA);
                                } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
                                   logger.info(e.getMessage());
                                }
                                return test;
                            })
                            .map(user -> {
                                JsonObject test = new JsonObject();
                                try {
                                    test = this.decode(user, CORREO);
                                } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
                                    logger.info(e.getMessage());
                                }
                                return new SesionDTO(test);
                            }).collect(Collectors.toList());
                });
    }

    private Future<Integer> agregarAdicional(JsonObject argumentos) {
        return this.client.preparedQuery("INSERT INTO adicional(id_carrera, semestre, id_usuario) VALUES ($1, $2, $3)")
                .execute(Tuple.of(argumentos.getJsonObject(USUARIO).getJsonObject(ADICIONAL).getInteger("carrera"), argumentos.getJsonObject(USUARIO).getJsonObject(ADICIONAL).getInteger("semestre"), argumentos.getInteger(ID_USUARIO)))
                .map(r -> argumentos.getInteger(ID_USUARIO));
    }


    private Future<JsonObject> codificar(JsonObject argumentos, String llave) {
        try {
            Key aesKey = new SecretKeySpec(SECRET_CODE.getBytes(), MODE_ALGORITHM);
            Cipher cipher = Cipher.getInstance(MODE_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(argumentos.getJsonObject(USUARIO).getString(llave).getBytes());
            argumentos.getJsonObject(USUARIO).put(llave, new String(Base64.getEncoder().encode(encrypted)));
            logger.info(argumentos);
            return Future.succeededFuture(argumentos);
        } catch (Exception e) {
            logger.info(e.getMessage());
            return Future.failedFuture(e.getCause());
        }
    }

    private Future<JsonObject> decodificar(JsonObject argumentos, String llave) {
        try {
            byte[] encryptedBytes = Base64.getDecoder().decode(argumentos.getJsonObject(USUARIO).getString(llave).replace("\n", ""));
            Key aesKey = new SecretKeySpec(SECRET_CODE.getBytes(), MODE_ALGORITHM);
            Cipher cipher = Cipher.getInstance(MODE_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            argumentos.getJsonObject(USUARIO).put(llave, new String(cipher.doFinal(encryptedBytes)));
            return Future.succeededFuture(argumentos);
        } catch (Exception e) {
            return Future.failedFuture(e.getCause());
        }

    }

    private JsonObject decode(JsonObject argumentos, String llave) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        byte[] encryptedBytes = Base64.getDecoder().decode(argumentos.getString(llave).replace("\n", ""));
        Key aesKey = new SecretKeySpec(SECRET_CODE.getBytes(), MODE_ALGORITHM);
        Cipher cipher = Cipher.getInstance(MODE_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        argumentos.put(llave, new String(cipher.doFinal(encryptedBytes)));
        return argumentos;
    }

}
