package umb.arquetipo.micro.tuples;

import io.vertx.core.json.JsonObject;
import io.vertx.sqlclient.Tuple;

import java.util.List;
import java.util.stream.Collectors;

import static umb.arquetipo.micro.common.Util.*;

public class TuplesLibro {
    TuplesLibro() {
    }

    public static Tuple nuevoLibroTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getString(TITULO),
                argumentos.getString(ISBN),
                argumentos.getInteger(PAGINAS),
                argumentos.getInteger(EXISTENCIA),
                argumentos.getInteger(EDICION),
                argumentos.getInteger(EDITORIAL),
                argumentos.getInteger(DISPONIBLES),
                argumentos.getInteger(PRESTADOS),
                argumentos.getInteger(APARTADOS)
        );
    }

    public static List<Tuple> relacionAutorTuple(JsonObject argumentos) {
        return argumentos.getJsonObject(LIBRO).getJsonArray(AUTORES)
                .stream()
                .map(Object::toString)
                .map(Integer::parseInt)
                .map(relation -> Tuple.of(argumentos.getInteger(ID_LIBRO), relation))
                .collect(Collectors.toList());
    }

    public static List<Tuple> relacionCategoriaTuple(JsonObject argumentos) {
        return argumentos.getJsonObject(LIBRO).getJsonArray(CATEGORIAS)
                .stream()
                .map(Object::toString)
                .map(Integer::parseInt)
                .map(relation -> Tuple.of(argumentos.getInteger(ID_LIBRO), relation))
                .collect(Collectors.toList());
    }

    public static Tuple actualizaLibroTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_LIBRO),
                argumentos.getJsonObject(LIBRO).getString(TITULO),
                argumentos.getJsonObject(LIBRO).getString(ISBN),
                argumentos.getJsonObject(LIBRO).getInteger(PAGINAS),
                argumentos.getJsonObject(LIBRO).getInteger(EXISTENCIA),
                argumentos.getJsonObject(LIBRO).getInteger(EDICION),
                argumentos.getJsonObject(LIBRO).getInteger(EDITORIAL),
                argumentos.getJsonObject(LIBRO).getInteger(DISPONIBLES),
                argumentos.getJsonObject(LIBRO).getInteger(PRESTADOS),
                argumentos.getJsonObject(LIBRO).getInteger(APARTADOS)
        );
    }
    public static Tuple idLibroTuple(JsonObject argumentos) {
        return Tuple.of(argumentos.getInteger(ID_LIBRO));
    }

}
