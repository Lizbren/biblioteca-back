package umb.arquetipo.micro.tuples;

import io.vertx.core.json.JsonObject;
import io.vertx.sqlclient.Tuple;

import static umb.arquetipo.micro.common.Util.*;

public class TuplesUsuarios {

    TuplesUsuarios(){
    }

    public static Tuple loginUsuarioTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getString(MATRICULA),
                argumentos.getString(CLAVE)
        );
    }
    public static Tuple nuevoUsuarioTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getString(NOMBRES),
                argumentos.getString(PRIMER_APELLIDO),
                argumentos.getString(SEGUNDO_APELLIDO),
                argumentos.getString(MATRICULA),
                argumentos.getBoolean(VIGENCIA),
                argumentos.getString(CORREO),
                argumentos.getString(CLAVE),
                argumentos.getInteger(TIPO_USUARIO),
                argumentos.getString(CICLO_ACTUAL)
        );
    }
}
