package umb.arquetipo.micro.tuples;

import io.vertx.core.json.JsonObject;
import io.vertx.sqlclient.Tuple;

import java.util.List;
import java.util.stream.Collectors;

import static umb.arquetipo.micro.common.Util.*;

public class TuplesCategorias {
    TuplesCategorias() {
    }

    public static Tuple nuevaCategoriaTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getJsonObject(CATEGORIA).getString(NOMBRE_MAY),
                argumentos.getJsonObject(CATEGORIA).getString(NOMBRE_MIN),
                argumentos.getJsonObject(CATEGORIA).getBoolean(VIGENTE)
        );
    }
    public static Tuple actualizaCategoriaTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_CATEGORIA),
                argumentos.getString(NOMBRE_MAY),
                argumentos.getString(NOMBRE_MIN)
        );
    }
    public static List<Tuple> actualizaVariasCategoriasTuple(JsonObject argumentos) {
        return argumentos.getJsonArray(CATEGORIAS)
                .stream()
                .map(Object::toString)
                .map(JsonObject::new)
                .map(TuplesCategorias::actualizaCategoriaTuple)
                .collect(Collectors.toList());
    }
    public static Tuple actualizaVigenciaCategoriaTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_CATEGORIA),
                argumentos.getBoolean(VIGENTE)
        );
    }
}
