package umb.arquetipo.micro.tuples;

import io.vertx.core.json.JsonObject;
import io.vertx.sqlclient.Tuple;

import java.util.List;
import java.util.stream.Collectors;

import static umb.arquetipo.micro.common.Util.*;

public class TuplesEditorial {
    TuplesEditorial() {
    }

    public static Tuple nuevaEditorialTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getJsonObject(EDITORIAL).getString(NOMBRE_MAY),
                argumentos.getJsonObject(EDITORIAL).getString(NOMBRE_MIN),
                argumentos.getJsonObject(EDITORIAL).getBoolean(VIGENTE)
        );
    }
    public static Tuple actualizaEditorialTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_EDITORIAL),
                argumentos.getString(NOMBRE_MAY),
                argumentos.getString(NOMBRE_MIN)
        );
    }
    public static List<Tuple> actualizaVariasEditorialTuple(JsonObject argumentos) {
        return argumentos.getJsonArray(EDITORIALES)
                .stream()
                .map(Object::toString)
                .map(JsonObject::new)
                .map(TuplesCategorias::actualizaCategoriaTuple)
                .collect(Collectors.toList());
    }
    public static Tuple actualizaVigenciaEditorialTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_EDITORIAL),
                argumentos.getBoolean(VIGENTE)
        );
    }
}
