package umb.arquetipo.micro.tuples;

import io.vertx.core.json.JsonObject;
import io.vertx.sqlclient.Tuple;

import java.util.List;
import java.util.stream.Collectors;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.common.Util.VIGENTE;

public class TuplesCarrera {
    TuplesCarrera() {
    }
    public static Tuple nuevaCarreraTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getJsonObject(CARRERA).getString(NOMBRE_MAY),
                argumentos.getJsonObject(CARRERA).getString(NOMBRE_MIN),
                argumentos.getJsonObject(CARRERA).getBoolean(VIGENTE)
        );
    }
    public static Tuple actualizaCarreraTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_CARRERA),
                argumentos.getString(NOMBRE_MAY),
                argumentos.getString(NOMBRE_MIN)
        );
    }
    public static List<Tuple> actualizaVariasCarrerasTuple(JsonObject argumentos) {
        return argumentos.getJsonArray(CARRERAS)
                .stream()
                .map(Object::toString)
                .map(JsonObject::new)
                .map(TuplesCategorias::actualizaCategoriaTuple)
                .collect(Collectors.toList());
    }
    public static Tuple actualizaVigenciaCarreraTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_CARRERA),
                argumentos.getBoolean(VIGENTE)
        );
    }
}
