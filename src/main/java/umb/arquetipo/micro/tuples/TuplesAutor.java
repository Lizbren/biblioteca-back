package umb.arquetipo.micro.tuples;

import io.vertx.core.json.JsonObject;
import io.vertx.sqlclient.Tuple;

import java.util.List;
import java.util.stream.Collectors;

import static umb.arquetipo.micro.common.Util.*;

public class TuplesAutor {
    public static Tuple nuevoAutorTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getString(NOMBRE_AUTOR),
                argumentos.getString(FECHA_NAC),
                argumentos.getString(PAIS)
        );
    }

    public static Tuple actualizaAutorTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_AUTOR),
                argumentos.getString(NOMBRE_AUTOR),
                argumentos.getString(FECHA_NAC),
                argumentos.getString(PAIS)
        );
    }

    public static List<Tuple> actualizaVariosAutoreTuple(JsonObject argumentos) {
        return argumentos.getJsonArray(AUTORES)
                .stream()
                .map(Object::toString)
                .map(JsonObject::new)
                .map(TuplesAutor::actualizaAutorTuple)
                .collect(Collectors.toList());
    }

    public static Tuple actualizaVigenciaAutorTuple(JsonObject argumentos) {
        return Tuple.of(
                argumentos.getInteger(ID_AUTOR),
                argumentos.getBoolean(VIGENTE)
        );
    }
    TuplesAutor() {
    }
}
