package umb.arquetipo.micro.tuples;

import io.vertx.core.json.JsonObject;
import io.vertx.sqlclient.Tuple;

import static umb.arquetipo.micro.common.Util.*;
import static umb.arquetipo.micro.common.Util.SOLICITUD;

public class TuplePrestamos {
    TuplePrestamos() {
    }

    public static Tuple prestamoTuple(JsonObject argumentos) {
        return Tuple.of(argumentos.getJsonObject(SOLICITUD).getInteger(ID_USUARIO),
                argumentos.getJsonObject(SOLICITUD).getInteger(ID_LIBRO),
                argumentos.getJsonObject(SOLICITUD).getBoolean("vencido"),
                argumentos.getJsonObject(SOLICITUD).getFloat("cuota"),
                argumentos.getJsonObject(SOLICITUD).getBoolean("autorizado"),
                argumentos.getJsonObject(SOLICITUD).getInteger("renovaciones"));
    }
}
