package umb.arquetipo.connect;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DataServiceConnectImpl {

    public PgPool open(Vertx vertx, JsonObject configuration, String name) {

        Map<String, String> properties = new HashMap<>();
        properties.put("application_name", name);

        return PgPool.pool(vertx, new PgConnectOptions()
                .setPort(configuration.getInteger("UMB_PG_DB_PORT"))
                .setHost(configuration.getString("UMB_PG_DB_HOST"))
                .setDatabase(configuration.getString("UMB_PG_DB_NAME"))
                .setUser(configuration.getString("UMB_PG_DB_USER"))
                .setPassword(configuration.getString("UMB_PG_DB_PASS"))
                .setProperties(properties), new PoolOptions()
                .setMaxSize(configuration.getInteger("UMB_PG_MAX_POOL_SIZE"))
                .setIdleTimeoutUnit(TimeUnit.SECONDS).setIdleTimeout(1)
        );
    }
}
